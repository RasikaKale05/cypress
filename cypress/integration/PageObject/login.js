/// <reference types="cypress" />
class login
{
    visit()
    {
         cy.visit('https://dev-cbt.dilipoakacademy.com/#/login') // open URL
    }
    fill_login_details(id,pass)
    {
       const field_id = cy.get('#mat-input-0').should('be.visible').should('be.enabled')
       field_id.type(id)
       const field_pass = cy.get('#mat-input-1').should('be.visible').should('be.enabled')
       field_pass.type(pass)
       return this
    }
    clicklogin()
    {
        const button = cy.get(".mat-primary[type='button']")
        button.click()
    }
    clickQuestion()
    {
        const question = cy.get("ul > :nth-child(3)")
        question.click()
    }   
    addQuestion()
    {
       const add_question = cy.get(".mat-primary[type='button']")
       add_question.click()// click on Add Question

    }
    FillReference_id(R_id)
    {
       const reference_id = cy.get("#mat-input-2").should('be.visible')
       reference_id.type(R_id)
    }
    SelectDificultyLevel()
    {
       const level_arrow = cy.get("#mat-select-1 > .mat-select-trigger > .mat-select-arrow-wrapper")
       level_arrow.click()//Difficulty level
       const level = cy.get("#mat-option-7").should('be.visible')
       level.click()
    }
    SelectQuetionFor()
    {
        const Question_for_arrow =cy.get("#mat-select-2 > .mat-select-trigger > .mat-select-arrow-wrapper")
       Question_for_arrow.click()//Question for
       const Question_for = cy.get("#mat-option-10").should('be.visible')
       Question_for.click()
    }
    SelectSection()
    {
       const section_arrow= cy.get("#mat-select-3 > .mat-select-trigger > .mat-select-arrow-wrapper")
       section_arrow.click()//Section
       const section= cy.get("#mat-option-14 ").should('be.visible')
       section.click()
    }
    SelectCategory()
      {  const Category_arrow = cy.get("#mat-select-4 > .mat-select-trigger > .mat-select-arrow-wrapper")
        Category_arrow.click()//Category
       const Category = cy.get("#mat-option-17").should('be.visible')
       Category.click()
       cy.wait(5000)
      }   
    SelectTag()
    {
       const tag_Arrow = cy.get("#mat-select-7 > .mat-select-trigger > .mat-select-arrow-wrapper")
       tag_Arrow.click()//Tag
       const tag1 = cy.get("#mat-option-20")
       tag1.click()
       const tag2 =cy.get("#mat-option-25")
       tag2.click()
        cy.wait(5000)
    }
    ClickNextButton()
    {
        const Next=cy.get("[title='Next'] > .mat-raised-button")
        Next.click({force: true})// Next button
    }
    Uploadimage()
    {
       const RadioButton= cy.get("#mat-radio-3 > .mat-radio-label > .mat-radio-container > .mat-radio-outer-circle").should('be.visible').should('not.be.checked')
       RadioButton.click({force: true})//Redio button
        // programmatically upload the Quetion image
        const image = 'Question.png';
        cy.get('input[type=file]').attachFile(image)
        cy.wait(5000)
        const Success =  cy.get('.dialog-error-action > app-iro-button > .mat-raised-button')
        Success.click()
        cy.wait(5000)
    }
    OptionA_Upload_image()
    {
        const dropdown= cy.get('#mat-select-8 > .mat-select-trigger > .mat-select-arrow-wrapper > .mat-select-arrow')
        dropdown.click()
        cy.wait(5000)
        const Img = cy.get('#mat-option-72 > .mat-option-text')
        Img.click()
        const image1 = 'Question.png';
         cy.get('[fxflex="75"] > input[type=file]').attachFile(image1)
         const Ok=cy.get('.dialog-error-action > app-iro-button > .mat-raised-button')
         Ok.click()
         cy.wait(1000)
    }
    OptionB_Text(value1)
    {
       const OptionB = cy.get('#mat-input-4').should('be.visible').should('be.enabled')
       OptionB.type(value1)
       cy.wait(1000)
    }
    OptionC_Text(value2)
    {
        const OptionC = cy.get('#mat-input-5').should('be.visible').should('be.enabled')
        OptionC.type(value2)
    }

    ClickAnswer()
    {
        const ans =cy.get('#mat-checkbox-2')
        ans.click()
    }
    Add_Justification()
    {
        cy.get('iframe#mce_1_ifr').then(function($iframe){
            const iframecontent = $iframe.contents().find('body')
            cy.wrap(iframecontent).type('No Justification')
         })
    }
    Save_to_Darft()
    {
        const next = cy.get('[title="Next"] > .mat-raised-button')
        next.click()
        const Save = cy.get('[colored="accent"] > .mat-raised-button > .mat-button-wrapper')
        Save.click()
    }

}
    export default login
